<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\View;

class Client extends Model
{
    public function credits()
    {
        return $this->hasMany('App\Credit', 'client_id', 'id');
    }
    //Scope
    public function scopeAccepted($query)
    {
        return $query->where('status', '=', 1);
    }
    public function scopeOfStatus($query, $status)
    {
        return $query->where('status', $status);
    }
    //Mutators
    public function getBirthdayAttribute($value)
    {
        return date('d.m.Y', strtotime($value));
    }
    public static function makeOperationsList($id)
    {
        $operations = '';
        foreach (Client::$operations as $op) {
            $operations = $operations.'<a href="'.action($op['action'],['id'=>$id]).'" title="'.$op['text'].'"><span class="'.$op['icon'].' text-'.$op['color'].'" aria-hidden="true">&nbsp;</span></a>';
        }
        return $operations;
    }

    protected $guarded = ['id','office_id'];

    public static $labels = [
        'id' => 'ИД',
        'last_name'=> 'Фамилия',
        'first_name' => 'Имя',
        'patronymic' => 'Отчество',
        'birthday' => 'Дата рождения',
        'mobile_phone' => 'Сотовый телефон',
        'email' => 'Электронная почта',
        'status' => 'Статус',
        'operations' => 'Операции'
    ];

    public static $operations = [
        'show' => [
            'text' => 'Информация',
            'icon' => 'glyphicon glyphicon-info-sign',
            'color' => 'info',
            'action' => 'Client@show'
        ],
        'statistic' => [
            'text' => 'Стастистика',
            'icon' => 'glyphicon glyphicon-stats',
            'color' => 'warning',
            'action' => 'Client@info'
        ],
        'payments' => [
            'text' => 'Платежи',
            'icon' => 'glyphicon glyphicon-ruble',
            'color' => 'default',
            'action' => 'Client@show'
        ],

    ];

    public static $status = [
        0 => [
            'title' => 'проходит проверку',
            'color' => 'warning'
        ],
        1 => [
            'title' => 'активен',
            'color' => 'success'
        ],
        2 => [
            'title' => 'отказ',
            'color' => 'danger'
        ],
    ];
}
