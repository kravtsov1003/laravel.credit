<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    public function creditType()
    {
        return $this->hasOne('App\Credit', 'credit_type', 'id');
    }

    public static $label = [
        'id' => 'ИД',
        'date' => 'Дата',
        'credit' => 'Договор',
        'sum' => 'Сумма',
        'days' => 'Дней',
        'percent' => 'Процент',
        'payment_date' => 'Дата платежа',
        'sum_total' => 'Итог',
        'status' => 'Статус'
    ];
    public static $label_grid = [
        'id' => 'ИД',
        'date' => 'Дата',
        'contract' => 'Договор',
        'sum' => 'Сумма',
        'days' => 'Дней',
        'percent' => 'Процент',
        'payment_date' => 'Дата платежа',
        'sum_total' => 'Итог',
        'status' => 'Статус'
    ];

    public static function generateContract()
    {
        $date = date('Y-m-d');
        $credits = Credit::where('created_at', 'like', "$date%");
        $count = $credits->count() + 1;
        return $count . "-" . date('Y-m-d');
    }

    public static function calculate($type, $date, $days, $sum)
    {
        $payment_date = date('Y-m-d',strtotime($date.'+'.$days.'days'));
        if ($date <> date('Y-m-d') and $payment_date > date('Y-m-d')) {
            $payment_date = date('Y-m-d');
            $days = Credit::diffdays($date, date('Y-m-d'));
        }

        $debt = Credit::debt($type,$payment_date,$sum);



        $typeCredit = Credittype::findOrFail($type);
        $result = $typeCredit->toArray();
        $result['date'] = $date;
        $result['sum'] = $sum;
        $result['days'] = $days;
        $result['percent'] = $sum / 100 * $typeCredit['rate'] * $days;
        $result['payment_date'] = $payment_date;
        $result = array_merge($result,$debt);
        return $result;
    }

    public static function currentState($credit)
    {
        $payment_date = $credit->payment_date;
        if($credit->payment_date > date('Y-m-d')){
            $payment_date = date('Y-m-d');
        }
        $debt = Credit::debt($credit->credit_type,$credit->payment_date,$credit->sum);
        $days = Credit::diffdays($credit->date,$payment_date);
        $typeCredit = Credittype::findOrFail($credit->credit_type);
        $result = $typeCredit->toArray();
        $result['date'] = $credit->date;
        $result['sum'] = $credit->sum;
        $result['days'] = $days;
        $result['percent'] =  $result['sum'] / 100 * $typeCredit['rate'] * $days;
        $result['payment_date'] = $payment_date;
        $result = array_merge($result,$debt);

        return $result;
    }

    public static function debt($type, $payment_date, $sum)
    {
        $result = array();
        $typeCredit = Credittype::find($type);
        $debt_days = Credit::diffdays($payment_date, date('Y-m-d'));
        if ($debt_days > 0) {
            $result['debt_days'] = $debt_days;
            $result['debt_percent'] = $sum / 100 * $typeCredit->delay_percent * $result['debt_days'];
        } else {
            $result['debt_days'] = 0;
            $result['debt_percent'] = 0;
        }
        if ($result['debt_days'] > $typeCredit['delay_days']) {
            $result['debt_fine'] = $typeCredit['delay_owing_sum'];
        } else {
            $result['debt_fine'] = 0;
        }
        return $result;
    }

    public static function diffdays($date_start, $date_end)
    {
        return (strtotime($date_end) - strtotime($date_start)) / 86400;
    }

    public static $status = [
        0 => [
            'title' => 'открыт',
            'color' => 'success'
        ],
        1 => [
            'title' => 'закрыт',
            'color' => 'error'
        ],
        2 => [
            'title' => 'продлен',
            'color' => 'warning'
        ],
        3 => [
            'title' => 'по факту',
            'color' => 'primary'
        ],
    ];
}
