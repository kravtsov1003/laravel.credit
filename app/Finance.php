<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Finance extends Model
{
    public static $status = [
        0 => [
            'title' => '������',
            'color' => 'success'
        ],
        1 => [
            'title' => '������',
            'color' => 'error'
        ],
        2 => [
            'title' => '�������',
            'color' => 'warning'
        ],
        3 => [
            'title' => '�� �����',
            'color' => 'primary'
        ],
        4 => [
            'title' => '���������',
            'color' => 'info'
        ],
    ];

    public static $type = [
        0 => [
            'title' => '������',
            'color' => 'success'
        ],
        1 => [
            'title' => '������',
            'color' => 'error'
        ]
    ];

    public static function insert($date,$body,$percent,$type,$status,$contract,$client){
        $finance = new Finance();
        $finance->payment_date = $date;
        $finance->body = $body;
        $finance->percent = $percent;
        $finance->type = $type;
        $finance->status = $status;
        $finance->contract = $contract;
        $finance->client = $client;
        $finance->total = $body+$percent;
        return $finance;

    }

    protected $table = 'finance';
}
