<?php

namespace App\Http\Controllers;

use App\Credit;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class Client extends Controller
{
    public function index(){

        $clients = \App\Client::all();
        return View::make('client.index')
            ->with('clients',$clients);
    }

    public function show($id){

        $client = \App\Client::findOrFail($id);
        $credits = $client->credits;
        $infoBlock = null;
        $openCredit = Credit::where('client_id',$id)->where('status',0)->first();
        if ($openCredit) {
            $openCreditInfo = Credit::currentState($openCredit);
            $infoBlock = View::make('credit.info')
                ->with('data',$openCreditInfo);
        }


        return View::make('client.show')
            ->with('client',$client)
            ->with('credits',$credits)
            ->with('infoblock',$infoBlock);
    }

    public function info($id){
        $client = \App\Client::findOrFail($id);
        $credits = $client->credits;
        return View::make('client.info')
            ->with('client',$client)
            ->with('credits',$credits);
    }

}
