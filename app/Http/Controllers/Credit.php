<?php

namespace App\Http\Controllers;

use App\Client;
use App\Credittype;
use App\Finance;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class Credit extends Controller
{
    public function open($id){
        $client  = Client::findOrFail($id);
        $credit = \App\Credit::where('client_id',$id)->orderBy('payment_date','desc')->first();
        $typeCredit = Credittype::getList();
        return View::make('credit.open')
            ->with('client',$client)
            ->with('credit',$credit)
            ->with('typeCredit',$typeCredit)
            ->with('userScripts',array('open-credit.js'));
    }
    public function open_store(Request $request){

        $data = $request->all();

        $parameters = \App\Credit::calculate($data['inputTC'],date('Y-m-d'),$data['inputDays'],$data['inputSum']);
        $v = Validator::make($request->all(), [
            'inputSum' => 'required|numeric|max:'.$parameters['max_sum'].'|min:'.$parameters['min_sum'],
            'inputDays' => 'required|numeric|min:1|max:365',
        ]);

        if ($v->fails())
        {
            return redirect()->back()->withErrors($v->errors())->withInput();
        }
        $credit = new \App\Credit();
        $credit->client_id = $data['client_id'];
        $credit->credit_type = $data['inputTC'];
        $credit->contract = \App\Credit::generateContract();
        $credit->date = date('Y-m-d');
        $credit->payment_date = $parameters['payment_date'];
        $credit->sum = $data['inputSum'];
        $credit->percent = $parameters['percent'];
        $credit->sum_total = $parameters['sum'] + $parameters['percent'];
        $credit->save();
        $finance = Finance::insert(date('Y-m-d H:i:s'),$data['inputSum'],$parameters['percent'],1,0,$credit->id,$credit->client_id);

        try{
            $finance->save();
        }
        catch(\Exception $e){
            $credit->delete();
            return redirect()->back()->withErrors(['Транзакция не прошла.'])->withInput();
        }


        return redirect(action('Document@open',$credit->id));
    }

    public function close($id){
        $client  = Client::findOrFail($id);
        $credit = \App\Credit::where('client_id',$id)->where('status',0)->first();
        $days = \App\Credit::diffdays($credit->date,$credit->payment_date);
        $parameters = \App\Credit::calculate($credit->credit_type,$credit->date,$days,$credit->sum);

        $typeCredit = Credittype::getList();
        return View::make('credit.close')
            ->with('client',$client)
            ->with('credit',$credit)
            ->with('typeCredit',$typeCredit)
            ->with('parameters',$parameters)
            ->with('userScripts',array('open-credit.js'));
    }

    public function close_store($id){
        $credit = \App\Credit::findOrFail($id);
        $credit->status = 1;
        $credit->save();
        $parameters = \App\Credit::calculate($credit->credit_type,$credit->date,$credit->days,$credit->sum,2);


        $credit = new \App\Credit();
        $credit->client_id = $data['client_id'];
        $credit->credit_type = $data['inputTC'];
        $credit->contract = \App\Credit::generateContract();
        $credit->date = date('Y-m-d');
        $credit->payment_date = $parameters['payment_date'];
        $credit->sum = $data['inputSum'];
        $credit->percent = $parameters['percent'];
        $credit->sum_total = $parameters['sum'] + $parameters['percent'];
        $credit->save();
        $finance = Finance::insert(date('Y-m-d H:i:s'),$data['inputSum'],$parameters['percent'],1,0,$credit->id,$credit->client_id);

        try{
            $finance->save();
        }
        catch(\Exception $e){
            $credit->delete();
            return redirect()->back()->withErrors(['Транзакция не прошла.'])->withInput();
        }


        return redirect(action('Document@open',$credit->id));
    }
}
