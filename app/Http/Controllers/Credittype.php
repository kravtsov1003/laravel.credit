<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class Credittype extends Controller
{
    public function getTypeCredit(Request $request){
        $typeCredit = \App\Credittype::find($request->id)->toArray();
        $typeCredit['date']=date('Y-m-d');
        return response()->json($typeCredit, 200);
    }

}
