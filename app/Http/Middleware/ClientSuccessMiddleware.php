<?php

namespace App\Http\Middleware;

use App\Client;
use Closure;
use Illuminate\Support\Facades\View;

class ClientSuccessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = $request->route()->getParameter('id');
        $client = Client::findOrFail($id);
        if ($client->status <> 1)
        {
            return View::make('errors.main')
                ->with('client',$client)
                ->with('text','Ошибка доступа.')
                ->with('desc','Статус клиента не позволяет выполнить данную операцию.');
        }
        return $next($request);
    }
}
