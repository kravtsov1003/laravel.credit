/**
 * Created by kkravtsov on 22.12.2015.
 */
$(document).ready(function () {
    $('#btnSaveSettings').on('click', function () {
        var $btn = $(this).button('loading');
        var token = document.getElementById('token').value
        $btn.button('reset');
        $.ajax({
            method: "POST",
            headers: {'X-CSRF-TOKEN': token},
            url: "/saveSettings",
            data: {name: "test", address: "test2"}
        }).done(function (msg) {
            alert("Data Saved: " + msg);
            $btn.button('reset');
        });
    });
});
function getWeekName(number){
    weekdayname = ['Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'];
    return weekdayname[number];
}

function getWeekColor(number){
    weekdaycolor = ['has-error','has-success','has-success','has-success','has-success','has-success','has-warning'];
    return weekdaycolor[number];
}