function calculateCredit(){
    $('#alert-open-credit-block .alert-open-credit').remove();
    var $_token = $('#_token').val();
    var typeCredit = $('#inputTC').val();
    var sum = $('#inputSum').val();
    var days = $('#inputDays').val();
    var message = '';

    $.ajax({
        method: "POST",
        url: "/credittype/get",
        data: {id: typeCredit, _token: $_token}
    })
        .done(function (data) {
            paramCredit = data;
            if (sum > 0 && days > 0) {
                percent = parseInt(sum / 100 * paramCredit['rate'] * days, 10);
                summa_total = parseInt(percent, 10) + parseInt(sum, 10);
                today = new Date(paramCredit['date']);
                datepay = new Date(paramCredit['date']);
                datepay.setDate(today.getDate()+parseInt(days));
                $('#outputRate').val(paramCredit['rate']+' %');
                $('#outputPercent').val(percent+' руб.');
                $('#outputDate').val(datepay.format("dd.mm.yyyy"));
                $('#outputDateWeek').val(getWeekName(datepay.getDay()));
                $('#weeknamefield').removeClass('has-success').removeClass('has-warning').removeClass('has-error').addClass(getWeekColor(datepay.getDay()))
                $('#outputTotal').val(summa_total+' руб.');
            } else {
                message = 'Заполните все поля!';
                $('#alert-open-credit-block').append("<div class='alert alert-warning alert-dismissible alert-open-credit' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Ошибка!</strong> " + message + " </div>");
            }
        })
        .fail(function() {
            alert( "Произошла ошибка при загрузке параметров" );
        })


}