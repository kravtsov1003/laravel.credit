@extends('layouts/app')
@section('content')

    <table class="table table-hover table-consider">
        <thead>
        <tr>
            @foreach(\App\Client::$labels as $label=>$val)
                <th>{{$val}}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach($clients as $c)
            <tr>
                @foreach(\App\Client::$labels as $label=>$value)
                    @if($label == 'operations')
                        <td>{!! \App\Client::makeOperationsList($c->id) !!}</td>
                    @elseif ($label == 'status')
                        <td>
                            <span class="text-{{\App\Client::$status[$c->$label]['color']}}">{{\App\Client::$status[$c->$label]['title']}}</span>
                        </td>
                    @else
                        <td>{{$c->$label}}</td>
                    @endif
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection