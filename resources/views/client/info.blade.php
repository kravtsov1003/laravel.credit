@extends('template')
@section('main')
    <div class="row">
        <div class="col-lg-6 col-sm-6 col-xs-12">
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object" src="{{asset('img/client/udefined.png')}}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">{{$client->last_name.' '.$client->first_name.' '.$client->patronymic}}</h4>

                    <p>
                        Статус: <span class="text-{{\App\Client::$status[$client->status]['color']}}">{{\App\Client::$status[$client->status]['title']}}</span></br>
                        Возраст: <span>25 лет</span></br>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12">
            <table class="table table-condensed">
                <tr>
                    <td>Должен:</td>
                    <td><strong>5000 р.</strong></td>
                </tr>
                <tr>
                    <td>Всего отдал:</td>
                    <td><strong>15 000 р.</strong></td>
                </tr>
                <tr>
                    <td>Из них:</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Основной долг</td>
                    <td><strong>10 000 р.</strong></td>
                </tr>
                <tr>
                    <td>Процент</td>
                    <td><strong>5 000 р.</strong></td>
                </tr>
            </table>
        </div>
    </div>

<div class="row">
    <canvas id="myChart" width="400" height="200"></canvas>
    <script>

        var ctx = document.getElementById("myChart");
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                datasets: [{
                    label: '# of Votes',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                },
                    {
                        label: '# of Votes 2',
                        data: [10, 9, 13, 4, 1, 55],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    </script>

</div>

@endsection