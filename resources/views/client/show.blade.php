@extends('layouts/app')
@section('content')
    <div class="row">
        <div class="col-lg-8 col-sm-8 col-xs-12">
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object" src="{{asset('img/client/udefined.png')}}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">{{$client->last_name.' '.$client->first_name.' '.$client->patronymic}}</h4>

                    <p>
                        Статус: <span
                                class="text-{{\App\Client::$status[$client->status]['color']}}">{{\App\Client::$status[$client->status]['title']}}</span></br>
                        Возраст: <span>25 лет</span></br>

                    </p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-4 col-xs-12 small">
            {!! $infoblock !!}
        </div>
    </div>
    <hr>
    <!--Кредиты-->
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-hover">
                <thead>
                <tr>
                    @foreach(\App\Credit::$label_grid as $l=>$v)
                        <th>{{$v}}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach($credits as $c)
                    <tr>
                        @foreach(\App\Credit::$label_grid as $l=>$v)
                            @if($l=='status')
                                <td class="text-{{\App\Credit::$status[$c->$l]['color']}}">{{\App\Credit::$status[$c->$l]['title']}}</td>
                            @elseif($l=='sum' or $l=='sum_total' or $l=='percent')
                                <td>{{number_format($c->$l,0,'',' ')}} р.</td>
                            @elseif($l=='date' or $l=='payment_date')
                                <td>{{date('d.m.Y',strtotime($c->$l))}}</td>
                            @elseif($l=='days')
                                <td>{{\App\Credit::diffdays($c->date,$c->payment_date)}}</td>
                            @else

                                <td>{{$c->$l}}</td>
                            @endif
                        @endforeach
                    </tr>
                </tbody>
                @endforeach
            </table>

            <!--Панель управления кредитами-->
            <div class="btn-group">
                <a class="btn btn-sm btn-success"
                   @if(\App\Credit::where('client_id',$client->id)->where('status',0)->first() or $client->status == 0)  disabled
                   @else href="{{action('Credit@open',['id'=>$client->id])}}" @endif ><span
                            class="glyphicon glyphicon-plus-sign"></span> Оформить
                </a>
            </div>
            <div class="btn-group">
                <a class="btn btn-sm btn-danger"
                   @if(\App\Credit::where('client_id',$client->id)->where('status','<>',0)->first() or $client->status <> 0) href="{{action('Credit@close',['id'=>$client->id])}}"
                   @else  disabled @endif >
                    <span class="glyphicon glyphicon-plus-sign"></span> Закрыть
                </a>
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-sm btn-primary"><span
                            class="glyphicon glyphicon-arrow-right"></span> Продлить
                </button>
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-sm btn-primary"> По факту</button>
            </div>
            <!--END Панель управления кредитами-->
        </div>
    </div>
    <!--END Кредиты-->
    <hr>
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default panel-comment">
                <div class="panel-body">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore magna aliqua.
                </div>
                <div class="panel-footer">Рыбинск 28.12.2015 12:01</div>
            </div>

            <div class="panel panel-default panel-comment">
                <div class="panel-body">
                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat.
                </div>
                <div class="panel-footer">Рыбинск 28.12.2015 12:01</div>
            </div>
            <div class="text-right"><a href="#"><span class="glyphicon glyphicon-chevron-down"></span> Показать
                    все</a><span> | </span> <a href="#"><span class="glyphicon glyphicon-plus"></span> Добавить</a>
            </div>
        </div>
    </div>
@endsection