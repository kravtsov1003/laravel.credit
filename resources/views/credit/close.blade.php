@extends('layouts/app')
@section('content')
    <div class="page-header">
        <h1>
            <a href="{{action('Client@show',['id'=>$credit->id])}}">{{$client->last_name.' '.$client->first_name.' '.$client->patronymic}}</a>
            <small> закрыть</small>
        </h1>
    </div>
    @if (isset($errors))
        @foreach($errors->all() as $e)
            <div class="alert alert-danger" role="alert">{{$e}}</div>
        @endforeach
    @endif
    <div id="alert-open-credit-block"></div>
    @if($credit)
    <form class="form-horizontal" action="{{action('Credit@close_store',['id'=>$credit->id])}}" method="POST">

        <input id="_token" name="_token" type="hidden" value="{{ csrf_token() }}">
        <input id="client_id" name="client_id" type="hidden" value="{{ $client->id }}">

        <div class="form-group">
            <label for="inputDays" class="col-sm-2 control-label">Количество дней</label>
            <div class="col-sm-10">
                <p class="form-control-static text-primary">{{$parameters['days']}} </p>
            </div>
        </div>
        @if($parameters['debt_days']>0)
        <div class="form-group has-error">
            <label for="inputDays" class="col-sm-2 control-label">Задолженность дней</label>
            <div class="col-sm-10">
                <p class="form-control-static text-danger">{{$parameters['debt_days']}}</p>
            </div>
        </div>
        @endif
        <div class="form-group">
            <label for="inputSum" class="col-sm-2 control-label">Сумма</label>
            <div class="col-sm-10">
                <p class="form-control-static text-primary">{{number_format($parameters['sum'],2,',',' ')}} <span class="glyphicon glyphicon-rub"/> </p>
            </div>
        </div>
        <div class="form-group text">
            <label for="inputDays" class="col-sm-2 control-label">Процент</label>
            <div class="col-sm-10">
                <p class="form-control-static text-primary">{{number_format($parameters['percent'],2,',',' ')}} <span class="glyphicon glyphicon-rub"/> </p>
            </div>
        </div>
        @if($parameters['debt_percent']>0)
        <div class="form-group has-error">
            <label for="inputDays" class="col-sm-2 control-label">Задолженность процентов</label>
            <div class="col-sm-10">
                <p class="form-control-static text-danger">{{number_format($parameters['debt_percent'],2,',',' ')}} <span class="glyphicon glyphicon-rub"/></p>
            </div>
        </div>
        @endif
        <hr/>
        <div class="form-group">
            <label for="outputTotal" class="col-sm-2 control-label">Сумма к оплате</label>
            <div class="col-sm-10">
                <p class="form-control-static text-success">{{number_format($parameters['sum']+$parameters['percent']+$parameters['debt_percent']+$parameters['debt_fine'],2,',',' ')}} <span class="glyphicon glyphicon-rub"/></p>
            </div>
        </div>
        <div class="form-group pull-right">
            <div class="col-sm-12">
                <button type="submit" class="btn btn-success">Оформить</button>
            </div>
        </div>
    </form>
    @else
        <div class="alert alert-danger" role="alert">У клиента имеются открытые займы.</div>
        <a href="{{action('Client@show',['id'=>$client->id])}}"><span class="glyphicon glyphicon-circle-arrow-left"></span>&nbsp; Назад</a>
    @endif
@endsection
