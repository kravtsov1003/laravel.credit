<table class="table table-condensed">
    <tbody>
    <tr>
        <td>Сумма к оплате:</td>
        <td> {{$data['sum'] + $data['percent'] + $data['debt_percent'] + $data['debt_fine']}} <span class="glyphicon glyphicon-rub small"></span></td>
    </tr>
    <tr>
        <td>Из них:</td>
        <td></td>
    </tr>
    <tr>
        <td>Сумма основного долга:</td>
        <td> {{$data['sum']}} <span class="glyphicon glyphicon-rub small"></span></td>
    </tr>
    @if($data['percent']>0)
    <tr>
        <td>Процент за пользование ({{$data['days']}} {{\App\Stringusefull::plural_form($data['days'],array('день','дня','дней'))}}):</td>
        <td> {{$data['percent']}} <span class="glyphicon glyphicon-rub small"></span></td>
    </tr>
    @endif
    @if($data['debt_percent']>0)
    <tr>
        <td>Сумма задолженности:</td>
        <td> <span class="text-danger">{{$data['debt_percent']}}</span> <span class="glyphicon glyphicon-rub small"></span></td>
    </tr>
    @endif
    @if($data['debt_fine']>0)
    <tr>
        <td>Штраф:</td>
        <td> <span class="text-danger">{{$data['debt_fine']}}</span> <span class="glyphicon glyphicon-rub small"></span></td>
    </tr>
    @endif
    </tbody>
</table>