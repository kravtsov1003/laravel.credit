@extends('template')
@section('main')
    <div class="page-header">
        <h1>
            <a href="{{action('Client@show',['id'=>$client->id])}}">{{$client->last_name.' '.$client->first_name.' '.$client->patronymic}}</a>
            <small> открыть</small>
        </h1>
    </div>
    @if (isset($errors))
        @foreach($errors->all() as $e)
            <div class="alert alert-danger" role="alert">{{$e}}</div>
        @endforeach
    @endif
    <div id="alert-open-credit-block"></div>
    @if(!$credit or $credit->status<>0)
    <form class="form-horizontal" action="{{action('Credit@open_store')}}" method="POST">
        <input id="_token" name="_token" type="hidden" value="{{ csrf_token() }}">
        <input id="client_id" name="client_id" type="hidden" value="{{ $client->id }}">
        <div class="form-group">
            <label for="inputSum" class="col-sm-2 control-label">Сумма</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputSum" name="inputSum" placeholder="Введите сумму" value="{{Request::old('inputSum')}}">
            </div>
        </div>
        <div class="form-group">
            <label for="inputDays" class="col-sm-2 control-label">Количество дней</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputDays" name="inputDays" placeholder="Введите количество дней" value="{{Request::old('inputDays')}}">
            </div>
        </div>
        <div class="form-group">
            <label for="inputTC" class="col-sm-2 control-label">Выберите тип</label>

            <div class="col-sm-10">
                <select type="text" class="form-control" id="inputTC" name="inputTC" onchange="calculateCredit();" value="{{Request::old('inputTC')}}">
                    @foreach($typeCredit as $tc)
                        <option value="{{$tc->id}}">{{$tc->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="outputRate" class="col-sm-2 control-label">Ставка</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="outputRate" name="outputRate" disabled>
            </div>
        </div>
        <div class="form-group">
            <label for="outputPercent" class="col-sm-2 control-label">Процент</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="outputPercent" name="outputPercent" disabled>
            </div>
        </div>
        <div class="form-group">
            <label for="outputDate" class="col-sm-2 control-label">Дата платежа</label>

            <div class="col-sm-8">
                <input type="text" class="form-control" id="outputDate" name="outputDate" disabled>
            </div>
            <div id="weeknamefield" class="col-sm-2">
                <input type="text" class="form-control text-danger" id="outputDateWeek" name="outputDateWeek" disabled>
            </div>
        </div>
        <div class="form-group">
            <label for="outputTotal" class="col-sm-2 control-label">Сумма к оплате</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="outputTotal" name="outputTotal" disabled>
            </div>
        </div>
        <div class="form-group pull-right">
            <div class="col-sm-12">
                <button type="button" id="calculate" class="btn btn-warning" onclick="calculateCredit();">Рассчет</button>
                <button type="submit" class="btn btn-success">Оформить</button>
            </div>
        </div>
    </form>
    @else
        <div class="alert alert-danger" role="alert">У клиента имеются открытые займы.</div>
        <a href="{{action('Client@show',['id'=>$client->id])}}"><span class="glyphicon glyphicon-circle-arrow-left"></span>&nbsp; Назад</a>
    @endif
@endsection
