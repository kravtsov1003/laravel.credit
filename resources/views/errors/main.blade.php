@extends('template')
@section('main')
    <div class="page-header">
        <h2>
            <a href="{{action('Client@show',['id'=>$client->id])}}">{{$client->last_name.' '.$client->first_name.' '.$client->patronymic}}</a>
        </h2>
    </div>
    <h3 class="text-danger">{{$text}}</h3>
    <p>{{$desc}}</p>
@endsection
