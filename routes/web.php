<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/','HomeController@index');
Route::get('/home', 'HomeController@index');

Route::get('client','Client@index');

Route::get('client/show/{id}','Client@show');
Route::get('client/info/{id}','Client@info');
//Credit
Route::get('credit/open/{id}',[
    'middleware' => 'clientSuccess', 'uses' => 'Credit@open'
]);
Route::post('credit/open/store','Credit@open_store');
Route::get('credit/close/{id}','Credit@close');
Route::post('credit/close/store/{id}','Credit@close_store');
//CreditType
Route::post('credittype/get/','Credittype@getTypeCredit');
//Documents
Route::get('document/open/{id}','Document@open');
